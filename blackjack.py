import random
import os
import sys


def menu():
    choice_menu = str(input('Deseja jogar novamente? [s/n]\n'))
    if choice_menu.lower() == 'n':
        print('------------------------------Fim do jogo!---------------------------')
        sys.exit()
    if choice_menu.lower() == 's':
        os.system('cls' if os.name == 'nt' else 'clear')
        script_jogo()

def script_jogo():
    print('-------------------------------Seja bem-vindo ao Blackjack!!----------------------------------')
    print('Começando jogo...')
    cartas = list(range(2, 11)) + list(range(2, 11)) + list(range(2, 11)) + list(range(2, 11)) + 'A A A A'.split()
    jogador = []
    pc = []
    cartas_iniciais_pc(pc, cartas)
    for cartas_init in range(0,2):
        deck = random.choice(cartas)
        if deck == 'A':
            escolha_as_player(jogador)
        else:
            jogador.append(deck)
    print('Suas cartas iniciais são: {}'.format(jogador))
    print('A soma das suas cartas deu {}.'.format(sum(jogador)))
    tratativas_cartas(jogador, pc, cartas)

def tratativas_cartas(jogador, pc, cartas):
    if sum(jogador) == 21 and sum(pc) == 21:
        print('Ohhhhhh boy. Vcs empataram.')
        menu()
    if sum(pc) == 21:
        print('Droga! O seu oponente venceu, ele fez 21 :c')
        menu()
    if sum(jogador) == 21:
        print('Parabéns! Você ganhou essa rodada (:')
        menu()
    if sum(pc) > 21:
        print('Weeeeeeeeeee! Você ganhou! Seu oponente estourou o 21.')
        menu()
    if sum(jogador) > 21:
        print('Poxa, você perdeu essa rodada :c')
        menu()
    if sum(jogador) < 21: #VER ISSO AQUI
        escolha_jogador = str(input(('Deseja mais uma carta? [s/n]\n')))
        if escolha_jogador.lower() == 'n':
            tratativas_end_game(jogador, pc)
        if escolha_jogador.lower() == 's':
            print('O seu oponente está jogando...')
            if sum(pc) <= 17:
                dar_carta_pc(pc, cartas)
                print('O seu oponente continuou e pediu mais uma carta...')
                print('As cartas do seu oponente são: {}'.format(pc))
                print('A soma das cartas do seu oponente deu {}.\n'.format(sum(pc)))
                if sum(pc) >= 21:
                    tratativas_end_game(jogador, pc)
                if sum(jogador) < 21:
                    dar_carta_player(jogador, cartas)
                    print('Suas cartas são: {}'.format(jogador))
                    print('A soma das suas cartas deu {}.'.format(sum(jogador)))
                    if sum(jogador) >= 21:
                        tratativas_end_game(jogador, pc)
                    else:
                        tratativas_cartas(jogador, pc, cartas)
            if sum(pc) >=18:
                print('O seu oponente amarelou e escolheu parar. What a pussy...')
                dar_carta_player(jogador, cartas)
                print('Suas cartas são: {}'.format(jogador))
                print('A soma das suas cartas deu {}.'.format(sum(jogador)))
                if sum(jogador) > 21:
                    tratativas_end_game(jogador, pc)
                else:
                    tratativas_cartas(jogador, pc, cartas)

def tratativas_end_game(jogador, pc):
    if sum(jogador) == sum(pc):
        print('Ohhhhhh boy. Vcs empataram.')
        menu()
    if sum(pc) > sum(jogador) and sum(pc) <= 21:
        print('Seu oponente venceu, poxa :c')
        menu()
    if sum(jogador) > sum(pc) and sum(jogador) <=21:
        print('Parabéns! Você ganhou essa rodada (:')
        menu()
    if sum(pc) > 21:
        print('Weeeeeeeeeee! Você ganhou! Seu oponente estourou o 21.')
        menu()
    if sum(jogador) > 21:
        print('Poxa, você perdeu essa rodada :c')
        print('A soma das suas cartas deu {}.'.format(sum(jogador)))
        menu()

def cartas_iniciais_pc(pc, cartas):
    for cartas_init in range(0,2):
        deck = random.choice(cartas)
        if deck == 'A':
            deck_as = 11
            pc.append(deck_as)
        else:
            pc.append(deck)
    print('As cartas iniciais do seu oponente são: {}'.format(pc))
    print('A soma das cartas do seu oponente deu {}.\n'.format(sum(pc)))
    return pc

def escolha_as_player(jogador):
    escolha_as = input(str("Saiu um Ás. Deseja que ele seja 1 ou 11? [1/11]\n"))
    if escolha_as == '1':
        deck_as = 1
        jogador.append(deck_as)
    if escolha_as == '11':
        deck_as = 11
        jogador.append(deck_as)
    if escolha_as not in ['1', '11']:
        escolha_as_player(jogador)
    return jogador

def dar_carta_pc(pc, cartas):
    for cartas_round_2 in range(0,1):
        deck = random.choice(cartas)
        if deck == 'A':
            deck_as = 1
            pc.append(deck_as)
        else:
            pc.append(deck)
    return pc

def dar_carta_player(jogador, cartas):
    for cartas_round in range(0,1):
        deck = random.choice(cartas)
        if deck == 'A':
            escolha_as_player(jogador)
        else:
            jogador.append(deck)
    return jogador

script_jogo()
