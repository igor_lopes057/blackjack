import random
import os

class BlackJack:
    def menu(self):
        blackjack = BlackJack()
        choice_menu = str(input('Deseja jogar novamente? [s/n]\n'))
        if choice_menu.lower() == 's':
            os.system('cls' if os.name == 'nt' else 'clear')
            blackjack.script_jogo()
        if choice_menu.lower() == 'n':
            print('------------------------------Fim do jogo!---------------------------')

    def script_jogo(self):
        blackjack = BlackJack()
        jogador = []
        pc = []
        print('-------------------------------Seja bem-vindo ao Blackjack!!----------------------------------')
        print('Começando jogo...')
        cartas = list(range(2, 11)) + list(range(2, 11)) + list(range(2, 11)) + list(range(2, 11)) + 'A A A A'.split()
        for cartas_init in range(0,2):
            deck = random.choice(cartas)
            if deck == 'A':
                deck_as = 11
                pc.append(deck_as)
            else:
                pc.append(deck)
        #CARTAS INICIAIS
        print('As cartas iniciais do seu oponente são: {}'.format(pc))
        print('A soma das cartas do seu oponente deu {}.\n'.format(sum(pc)))
        for cartas_init in range(0,2):
            deck = random.choice(cartas)
            if deck == 'A':
                escolha_as = input(str("Saiu um Ás. Deseja que ele seja 1 ou 11? [1/11]\n"))
                if escolha_as == '1':
                    deck_as = 1
                    jogador.append(deck_as)
                if escolha_as == '11':
                    deck_as = 11
                    jogador.append(deck_as)
                if escolha_as not in ['1', '11']:
                    raise TypeError
            else:
                jogador.append(deck)
        print('Suas cartas iniciais são: {}'.format(jogador))
        print('A soma das suas cartas deu {}.'.format(sum(jogador)))
        #TRATATIVAS DAS CARTAS INICIAIS
        if sum(jogador) == 21 and sum(pc) == 21:
            print('Ohhhhhh boy. Vcs empataram.')
            blackjack.menu()
        if sum(pc) == 21:
            print('Droga! O seu oponente venceu, ele fez 21 :c')
            blackjack.menu()
        if sum(jogador) == 21:
            print('Parabéns! Você ganhou essa rodada (:')
            blackjack.menu()
        if sum(pc) > 21:
            print('Weeeeeeeeeee! Você ganhou! Seu oponente estourou o 21.')
            blackjack.menu()
        if sum(jogador) > 21:
            print('Poxa, você perdeu essa rodada :c')
            print('A soma das suas cartas deu {}.'.format(sum(jogador)))
            blackjack.menu()
            #ESCOLHA DO PLAYER SE ELE QUER OU NÃO MAIS CARTAS
        if sum(jogador) < 21:
            escolha_jogador = str(input(('Deseja mais uma carta? [s/n]\n')))
            if escolha_jogador.lower() == 'n':
                if sum(pc) > sum(jogador) and sum(pc) <= 21:
                    print('O seu oponente ganhou, cara :c')
                    blackjack.menu()
                if sum(pc) == sum(jogador):
                    print('Deu empate. Poxa :c')
                    blackjack.menu()
                if sum(pc) < sum(jogador) and sum(jogador) <= 21:
                    print('Weeeeeeeeeee! Você ganhou.')
                    blackjack.menu()
            if escolha_jogador.lower() == 's':
                print('O seu oponente está jogando...')
                if sum(pc) < 15:
                    for cartas_round_2 in range(0,1):
                        deck = random.choice(cartas)
                        if deck == 'A':
                            deck_as = 1
                            pc.append(deck_as)
                        else:
                            pc.append(deck)
                    print('O seu oponente continuou e pediu mais uma carta...')
                    print('As cartas do seu oponente são: {}'.format(pc))
                    print('A soma das cartas do seu oponente deu {}.\n'.format(sum(pc)))
                    if sum(pc) > 21:
                        print('Hahah! O seu oponente estourou. Você ganhou. Parabéns (:')
                        blackjack.menu()
                    if sum(pc) <= 21:
                        for cartas_round_2 in range(0,1):
                            deck = random.choice(cartas)
                            if deck == 'A':
                                escolha_as = input(str("Saiu um Ás. Deseja que ele seja 1 ou 11? [1/11]\n"))
                                if escolha_as == '1':
                                    deck_as = 1
                                    jogador.append(deck_as)
                                if escolha_as == '11':
                                    deck_as = 11
                                    jogador.append(deck_as)
                                if escolha_as not in ['1', '11']:
                                    raise TypeError
                            else:
                                jogador.append(deck)
                                print('Sua jogada...')
                                print('Suas cartas são: {}'.format(jogador))
                                print('A soma das suas cartas deu {}.'.format(sum(jogador)))
                                if sum(jogador) > 21:
                                    print('Poxa, que triste. Você estourou. O seu oponente ganhou :c')
                                    blackjack.menu()
                                if sum(pc) == 21 and sum(jogador) == 21:
                                    print('Eita! Deu empate :c')
                                    BlackJack.menu()
                                else:
                                    #ESCOLHA DO PLAYER SE ELE QUER OU NÃO MAIS CARTAS
                                    escolha_jogador = str(input(('Deseja mais uma carta? [s/n]\n')))
                                    if escolha_jogador.lower() == 'n':
                                        if sum(pc) > sum(jogador) and sum(pc) <= 21:
                                            print('O seu oponente ganhou, cara :c')
                                            blackjack.menu()
                                        if sum(pc) == sum(jogador):
                                            print('Deu empate. Poxa :c')
                                            blackjack.menu()
                                        if sum(pc) < sum(jogador) and sum(jogador) <= 21:
                                            print('Weeeeeeeeeee! Você ganhou.')
                                            blackjack.menu()
                                    if escolha_jogador.lower() == 's':
                                        print('O seu oponente está jogando...')
                                        if sum(pc) >= 18:
                                            print('Ele não quer pedir mais cartas.')
                                            print('As cartas do seu oponente são: {}'.format(pc))
                                            print('A soma das cartas do seu oponente deu {}.\n'.format(sum(pc)))
                                            for cartas_round_2 in range(0,1):
                                                deck = random.choice(cartas)
                                                if deck == 'A':
                                                    escolha_as = input(str("Saiu um Ás. Deseja que ele seja 1 ou 11? [1/11]\n"))
                                                    if escolha_as == '1':
                                                        deck_as = 1
                                                        jogador.append(deck_as)
                                                    if escolha_as == '11':
                                                        deck_as = 11
                                                        jogador.append(deck_as)
                                                    if escolha_as not in ['1', '11']:
                                                        raise TypeError
                                                else:
                                                    jogador.append(deck)
                                                    print('Sua jogada...')
                                                    print('Suas cartas são: {}'.format(jogador))
                                                    print('A soma das suas cartas deu {}.'.format(sum(jogador)))
                                                    if sum(jogador) > 21:
                                                        print('Poxa, que triste. Você estourou. O seu oponente ganhou :c')
                                                        blackjack.menu()
                                                    if sum(pc) == 21 and sum(jogador) == 21:
                                                        print('Eita! Deu empate :c')
                                                        BlackJack.menu()
                                        if sum(pc) < 18:
                                                for cartas_round_3 in range(0,1):
                                                    deck = random.choice(cartas)
                                                    if deck == 'A':
                                                        deck_as = 1
                                                        pc.append(deck_as)
                                                    else:
                                                        pc.append(deck)
                                                print('O seu oponente continuou e pediu mais uma carta...')
                                                print('As cartas do seu oponente são: {}'.format(pc))
                                                print('A soma das cartas do seu oponente deu {}.\n'.format(sum(pc)))
                                                if sum(pc) > 21:
                                                    print('Hahah! O seu oponente estourou. Você ganhou. Parabéns (:')
                                                    blackjack.menu()
                                                else:
                                                    jogador.append(deck)
                                                    print('Sua jogada...')
                                                    print('Suas cartas são: {}'.format(jogador))
                                                    print('A soma das suas cartas deu {}.'.format(sum(jogador)))
                                                    if sum(jogador) > 21:
                                                        print('Poxa, que triste. Você estourou. O seu oponente ganhou :c')
                                                        blackjack.menu()
                                                    if sum(pc) == 21 and sum(jogador) == 21:
                                                        print('Eita! Deu empate :c')
                                                        BlackJack.menu()
                if sum(pc) >= 18 and sum(pc) < 21:
                    print('O seu oponente amarelou e escolheu parar. What a pussy...')
                    print('As cartas do seu oponente são: {}'.format(pc))
                    print('A soma das cartas do seu oponente deu {}.\n'.format(sum(pc)))
                    for cartas_round_2 in range(0,1):
                        deck = random.choice(cartas)
                        if deck == 'A':
                            escolha_as = input(str("Saiu um Ás. Deseja que ele seja 1 ou 11? [1/11]\n"))
                            if escolha_as == '1':
                                deck_as = 1
                                jogador.append(deck_as)
                            if escolha_as == '11':
                                deck_as = 11
                                jogador.append(deck_as)
                            if escolha_as not in ['1', '11']:
                                raise TypeError
                        else:
                            jogador.append(deck)
                            print('Sua jogada...')
                            print('Suas cartas são: {}'.format(jogador))
                            print('A soma das suas cartas deu {}.'.format(sum(jogador)))
                            if sum(jogador) > 21:
                                print('Poxa, que triste. Você estourou. O seu oponente ganhou :c')
                                blackjack.menu()
                            if sum(pc) == 21 and sum(jogador) == 21:
                                print('Eita! Deu empate :c')
                                BlackJack.menu()
                            else:
                                escolha_jogador = str(input(('Deseja mais uma carta? [s/n]\n')))
                                if escolha_jogador.lower() == 'n':
                                    if sum(pc) > sum(jogador) and sum(pc) <= 21:
                                        print('O seu oponente ganhou, cara :c')
                                        blackjack.menu()
                                    if sum(pc) == sum(jogador):
                                        print('Deu empate. Poxa :c')
                                        blackjack.menu()
                                    if sum(pc) < sum(jogador) and sum(jogador) <= 21:
                                        print('Weeeeeeeeeee! Você ganhou.')
                                        blackjack.menu()
                                if escolha_jogador.lower() == 's':
                                    print('O seu oponente está jogando...')
                                    if sum(pc) >= 18:
                                        print('Ele não quer pedir mais cartas.')
                                        print('As cartas do seu oponente são: {}'.format(pc))
                                        print('A soma das cartas do seu oponente deu {}.\n'.format(sum(pc)))
                                        for cartas_round_2 in range(0,1):
                                            deck = random.choice(cartas)
                                            if deck == 'A':
                                                escolha_as = input(str("Saiu um Ás. Deseja que ele seja 1 ou 11? [1/11]\n"))
                                                if escolha_as == '1':
                                                    deck_as = 1
                                                    jogador.append(deck_as)
                                                if escolha_as == '11':
                                                    deck_as = 11
                                                    jogador.append(deck_as)
                                                if escolha_as not in ['1', '11']:
                                                    raise TypeError
                                            else:
                                                jogador.append(deck)
                                                print('Sua jogada...')
                                                print('Suas cartas são: {}'.format(jogador))
                                                print('A soma das suas cartas deu {}.'.format(sum(jogador)))
                                                if sum(jogador) > 21:
                                                    print('Poxa, que triste. Você estourou. O seu oponente ganhou :c')
                                                    blackjack.menu()
                                                if sum(pc) == 21 and sum(jogador) == 21:
                                                    print('Eita! Deu empate :c')
                                                    BlackJack.menu()
                                    if sum(pc) < 18:
                                            for cartas_round_3 in range(0,1):
                                                deck = random.choice(cartas)
                                                if deck == 'A':
                                                    deck_as = 1
                                                    pc.append(deck_as)
                                                else:
                                                    pc.append(deck)
                                            print('O seu oponente continuou e pediu mais uma carta...')
                                            print('As cartas do seu oponente são: {}'.format(pc))
                                            print('A soma das cartas do seu oponente deu {}.\n'.format(sum(pc)))
                                            if sum(pc) > 21:
                                                print('Hahah! O seu oponente estourou. Você ganhou. Parabéns (:')
                                                blackjack.menu()
                                            else:
                                                jogador.append(deck)
                                                print('Sua jogada...')
                                                print('Suas cartas são: {}'.format(jogador))
                                                print('A soma das suas cartas deu {}.'.format(sum(jogador)))
                                                if sum(jogador) > 21:
                                                    print('Poxa, que triste. Você estourou. O seu oponente ganhou :c')
                                                    blackjack.menu()
                                                if sum(pc) == 21 and sum(jogador) == 21:
                                                    print('Eita! Deu empate :c')
                                                    BlackJack.menu()
                if sum(pc) > 21:
                    print('Seu oponente estourou, hahaha. Parabéns. Você ganhou (:')
                    blackjack.menu()


BlackJack().script_jogo()
